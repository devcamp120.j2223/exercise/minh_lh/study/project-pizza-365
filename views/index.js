/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCombo = [
    {
        kichCo: "S",
        duongKinh: 20,
        suon: 2,
        salad: "200g",
        soLuongNuoc: 2,
        thanhTien: 150000,
    },
    {
        kichCo: "M",
        duongKinh: 25,
        suon: 4,
        salad: "300g",
        soLuongNuoc: 3,
        thanhTien: 200000,
    },
    {
        kichCo: "L",
        duongKinh: 30,
        suon: 8,
        salad: "500g",
        soLuongNuoc: 4,
        thanhTien: 250000,
    }
]
var gPizzaType = [
    {
        loaiPizza: "seafood",
        pizzaImg: "images/seafood.jpg",
        pizzaName: "OCEAN MANIA",
        pizzaTitle: "PIZZA HẢI SẢN XỐT MAYONAISE",
        pizzaInfo: "Xốt Cà Chua, Phô Mai Mozzarella, Tôm, Mực, Thanh Cua, Hành Tây."
    },
    {
        loaiPizza: "hawaii",
        pizzaImg: "images/hawaiian.jpg",
        pizzaName: "HAWAIIAN",
        pizzaTitle: "PIZZA DĂM BÔNG DỨA KIỂU HAWAII",
        pizzaInfo: "Xốt Cà Chua, Phô Mai Mozzarella, Thịt Dăm Bông, Thơm."
    },
    {
        loaiPizza: "bacon",
        pizzaImg: "images/bacon.jpg",
        pizzaName: "CHESSY CHICKEN BANCON",
        pizzaTitle: "PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI",
        pizzaInfo: "Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà Chua."
    },
]
var gComboSelect = {};
var gPizzaSelect = "";
var gFormUser = {
    hoTen: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    idVourcher: "",
    loiNhan: ""
};
var gSelectPerson = {};
var gDiscount = {
    id: "",
    maVoucher: "Không có mã voucher",
    phanTramGiamGia: "0",
    ghiChu: "",
    ngayTao: "",
    ngayCapNhat: ""
}
var gNewOrder = {};
const gURL_DRINK = "http://42.115.221.44:8080/devcamp-pizza365/drinks";
const gURL_VOUCHER = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/"
const gURL_ORDER = "http://42.115.221.44:8080/devcamp-pizza365/orders"

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
// hàm load page
$(document).ready(function () {
    // tự thêm card combo dựa vào obj combo toàn cục
    loadCardCombo();
    // tự thêm card pizza dựa vào obj pizza type toàn cục
    loadCardPizzaType();
    // load select nước uống
    loadDirkSelect();
    // hàm lắng nghe khi nhập trường input
    addEventListener();
    // debugger;
})
// hàm nhấn nút chọn combo
$(document).on("click", ".combo-select", function () {
    // gán thông tin của combo chọn vào biến toàn cục
    gComboSelect = getDataComboSelect(this);
    console.log(gComboSelect)
    // đổi màu nút
    changeBtnClick(this, ".combo-select");
})
// hàm nhấn nút chọn loại pizza
$(document).on("click", ".pizza-select", function () {
    gPizzaSelect = getDataPizzaSelect(this);
    console.log(gPizzaSelect)
    changeBtnClick(this, ".pizza-select");
})
// hàm nhấn nút gửi đơn
$(document).on("click", ".btn-send", function () {
    // lấy thông tin người dùng đã nhập và đã chọn vào 1 biến toàn cục
    GetDataPersonSelect();
    // kiểm tra thông tin người dùng đã chọn và đã nhập
    var vValidate = validate();
    if (vValidate) {
        // gọi api check voucher
        callApiGetVoucher();
        // đỗ dữ liệu vào modal
        loadOrderToModal();
        // hiện modal
        $("#order-modal").modal("show");
    }
})
// hàm nhấn nút tạo đơn
$(document).on("click", "#btn-create-order", function () {
    // call api tạo đơn hàng mới và gán order id vào modal
    callAPiCreateOrder();
    // ẩn modal order
    $("#order-modal").modal("hide");
    // hiện modal thông báo order id
    $("#modal-order-id").modal("show");
})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm tạo mới card combo dựa vào gcombo
function loadCardCombo() {
    for (var i = 0; i < gCombo.length; i++) {
        var vSize = "";
        var vPrice = 0;
        if (gCombo[i].kichCo === "S") {
            vSize = "(small)";
            vPrice = "150.000";
        }
        if (gCombo[i].kichCo === "M") {
            vSize = "(medium)";
            vPrice = "200.000";
        }
        if (gCombo[i].kichCo === "L") {
            vSize = "(large)";
            vPrice = "250.000";
        }
        let Card = `
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header bg-orange text-dark text-center">
                        <h3><b>${gCombo[i].kichCo} ${vSize}</b></h3>
                    </div>
                    <div class="card-body text-center">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Đường kính:&nbsp<b>${gCombo[i].duongKinh}cm</b></li>
                            <li class="list-group-item">Sườn nướng:&nbsp<b>${gCombo[i].suon}</b></li>
                            <li class="list-group-item">Salad:&nbsp<b>${gCombo[i].salad}</b></li>
                            <li class="list-group-item">Nước ngọt:&nbsp<b>${gCombo[i].soLuongNuoc}</b></li>
                            <li class="list-group-item">
                                <h1><b>${vPrice}</b></h1>VNĐ
                            </li>
                        </ul>
                    </div>
                    <div class="card-footer text-center">
                        <button class="btn bg-orange w-100 combo-select"><b>Chọn</b></button>
                    </div>
                </div>
                </div>        
        `
        document.querySelector(".combo").innerHTML += Card;
    }
}
// hàm tạo mới card pizza type dựa vòa gPizzaType
function loadCardPizzaType() {
    for (var i = 0; i < gPizzaType.length; i++) {
        let Card = `
        <div class="col-sm-4">
            <div class="card w-100" style="width: 18rem">
                <img src="${gPizzaType[i].pizzaImg}" class="card-img-top" />
                <div class="card-body">
                    <h3 class="d-none">${gPizzaType[i].loaiPizza}</h3>
                    <h5>${gPizzaType[i].pizzaName}</h5>
                    <p>${gPizzaType[i].pizzaTitle}</p>
                    <p>
                        ${gPizzaType[i].pizzaInfo}
                    </p>
                    <p>
                        <button class="btn bg-orange pizza-select w-100"><b>Chọn</b></button>
                    </p>
                </div>
            </div>
        </div>
        `
        document.querySelector(".pizza-type").innerHTML += Card;
    }
}
// load option nước uống vào select
function loadDirkSelect() {
    $.ajax({
        url: gURL_DRINK,
        type: "GET",
        success: function (res) {
            console.log(res);
            addOptionDrink(res);
        },
        error: function (res) {
            console.log(res.status);
        }
    })
}
// hàm lắng nghe khi nhập ô input
function addEventListener() {
    $(".combo-select").on("click", function () {
        $("#combo").find(".combo-danger").addClass("text-hide");
        $("#combo").find(".card").removeClass("border-danger");
    })
    $(".pizza-select").on("click", function () {
        $("#pizza").find(".combo-danger").addClass("text-hide");
        $("#pizza").find(".card").removeClass("border-danger");
    })
    $("#input-select").on("change", function () {
        $("#drink").find(".combo-danger").addClass("text-hide");
        $("#input-select").removeClass("is-invalid");
    })
    $("#inp-fullname").on("keyup", function () {
        if ($(this).val().trim() != "" && isNaN($(this).val().trim())) {
            $("#inp-fullname").removeClass("is-invalid");
            $("#inp-fullname").parent("").find("p").addClass("text-hide");
        }
    })
    $("#inp-email").on("keyup", function () {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($(this).val().trim())) {
            $("#inp-email").removeClass("is-invalid");
            $("#inp-email").parent("").find("p").addClass("text-hide");
        }
    })
    $("#inp-dien-thoai").on("keyup", function () {
        if (isNaN($(this).val().trim()) == false && $(this).val().trim().length == 10) {
            $("#inp-dien-thoai").removeClass("is-invalid");
            $("#inp-dien-thoai").parent("").find("p").addClass("text-hide");
        }
    })
    $("#inp-dia-chi").on("keyup", function () {
        if ($(this).val().trim() != "") {
            $("#inp-dia-chi").removeClass("is-invalid");
            $("#inp-dia-chi").parent("").find("p").addClass("text-hide");
        }
    })
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm lấy thông tin combo được chọn
function getDataComboSelect(paramBtn) {
    var vCombo
    var vChar = $(paramBtn).parents(".card").find(".card-header h3 b").text()
    for (var i = 0; i < gCombo.length; i++) {
        if (vChar.startsWith(gCombo[i].kichCo)) {
            vCombo = gCombo[i]
        }
    }
    return vCombo
}
// hàm lấy thông tin pizza type được chọn
function getDataPizzaSelect(paramBtn) {
    var vPizza
    var vChar = $(paramBtn).parents(".card").find(".card-body h3").text()
    for (var i = 0; i < gPizzaType.length; i++) {
        if (vChar === (gPizzaType[i].loaiPizza)) {
            vPizza = gPizzaType[i].loaiPizza
        }
    }
    return vPizza
}
// hàm đổi màu khi nhấn nút
function changeBtnClick(paramBtn, paramALLBtn) {
    $(paramALLBtn).parent().find("button").removeClass("bg-orange");
    $(paramALLBtn).parent().find("button").removeClass("btn-success");
    $(paramALLBtn).parent().find("button").addClass("bg-orange");
    $(paramBtn).parent().find("button").removeClass("bg-orange");
    $(paramBtn).parent().find("button").addClass("btn-success");
}
// hàm đổ thông tin nước uống vào select
function addOptionDrink(paramDrink) {
    for (var i = 0; i < paramDrink.length; i++) {
        $("#input-select").append(`<option value="${paramDrink[i].maNuocUong}">${paramDrink[i].tenNuocUong}</option>`)
    }
}
// hàm lấy thông tin của người dùng đã chọn và đã nhập
function GetDataPersonSelect() {
    GetDataForm();
    var loaiPizza = gPizzaSelect;
    var idLoaiNuocUong = $("#input-select").val();
    gSelectPerson = { ...gComboSelect, loaiPizza, idLoaiNuocUong, ...gFormUser }
    console.log(gSelectPerson)
    return gSelectPerson
}
// hàm lấy thông tin trên form
function GetDataForm() {
    gFormUser.hoTen = $("#inp-fullname").val().trim();
    gFormUser.email = $("#inp-email").val().trim();
    gFormUser.soDienThoai = $("#inp-dien-thoai").val().trim();
    gFormUser.diaChi = $("#inp-dia-chi").val().trim();
    gFormUser.idVourcher = $("#inp-voucher").val().trim();
    gFormUser.loiNhan = $("#inp-message").val().trim();
}
// hàm kiểm tra thông tin người dùng đã chọn và đã nhập
function validate() {
    if (gSelectPerson.kichCo == undefined) {
        console.log("chưa chọn size combo")
        $("#modal-combo").modal("show");
        $("#combo").find(".combo-danger").removeClass("text-hide");
        $("#combo").find(".card").addClass("border-danger");
        window.location.href = "index.html#combo";
        return false;
    }
    if (gSelectPerson.loaiPizza == "") {
        console.log("chưa chọn loại pizza");
        $("#modal-pizza").modal("show");
        $("#pizza").find(".combo-danger").removeClass("text-hide");
        $("#pizza").find(".card").addClass("border-danger");
        window.location.href = "index.html#pizza";
        return false;
    }
    if (gSelectPerson.idLoaiNuocUong == "none") {
        console.log("chưa chọn loại nước uống");
        $("#modal-drink").modal("show");
        $("#drink").find(".combo-danger").removeClass("text-hide");
        $("#input-select").addClass("is-invalid");
        window.location.href = "index.html#drink";
        return false;
    }
    if (gSelectPerson.hoTen != "" && isNaN(gSelectPerson.hoTen)) {
        $("#inp-fullname").removeClass("is-invalid");
        $("#inp-fullname").parent("").find("p").addClass("text-hide");
    }
    else {
        console.log("chưa nhập tên!");
        $("#inp-fullname").addClass("is-invalid");
        $("#inp-fullname").parent("").find("p").removeClass("text-hide");
        return false;
    }
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(gSelectPerson.email)) {
        $("#inp-email").removeClass("is-invalid");
        $("#inp-email").parent("").find("p").addClass("text-hide");
    }
    else {
        console.log("chưa nhập email đúng");
        $("#inp-email").addClass("is-invalid");
        $("#inp-email").parent("").find("p").removeClass("text-hide");
        return false;
    }
    if (isNaN(gSelectPerson.soDienThoai) == false && gSelectPerson.soDienThoai.length == 10) {
        $("#inp-dien-thoai").removeClass("is-invalid");
        $("#inp-dien-thoai").parent("").find("p").addClass("text-hide");
    }
    else {
        console.log("chưa nhập số điện thoại!");
        $("#inp-dien-thoai").addClass("is-invalid");
        $("#inp-dien-thoai").parent("").find("p").removeClass("text-hide");
        return false;
    }
    if (gSelectPerson.diaChi != "") {
        $("#inp-dia-chi").removeClass("is-invalid");
        $("#inp-dia-chi").parent("").find("p").addClass("text-hide");
    }
    else {
        console.log("chưa nhập địa chỉ!");
        $("#inp-dia-chi").addClass("is-invalid");
        $("#inp-dia-chi").parent("").find("p").removeClass("text-hide");
        return false;
    }
    return true
}
// hàm lấy voucher từ api
function callApiGetVoucher() {
    $.ajax({
        url: gURL_VOUCHER + gSelectPerson.idVourcher,
        type: "GET",
        async: false,
        success: function (res) {
            gDiscount = res;

        },
        error: function (res) {
            console.log("status " + res.status)
        }
    })
}
// hàm đổ dữ liệu order vào modal
function loadOrderToModal() {
    $("#inp-fullname-order").val(gSelectPerson.hoTen);
    $("#inp-sdt").val(gSelectPerson.soDienThoai);
    $("#inp-dia-chi-order").val(gSelectPerson.diaChi);
    $("#inp-loi-nhan-order").val(gSelectPerson.loiNhan);
    $("#inp-ma-giam-gia").val(gSelectPerson.idVourcher)
    $("#inp-thong-tin-chi-tiet").val(
        `Xác nhận:
        Họ tên: ${gSelectPerson.hoTen}, Số điện thoại: ${gSelectPerson.soDienThoai}, Địa chỉ: ${gSelectPerson.diaChi},
        Pizza: combo ${gSelectPerson.kichCo} vị ${gSelectPerson.loaiPizza}, Sườn nướng: ${gSelectPerson.suon}, Nước: ${gSelectPerson.soLuongNuoc} ${gSelectPerson.idLoaiNuocUong}
        Giá combo: ${gSelectPerson.thanhTien}VNĐ, Mã giảm giá: ${gDiscount.maVoucher}
        Thành tiền: ${(gSelectPerson.thanhTien - (gSelectPerson.thanhTien * gDiscount.phanTramGiamGia / 100))}VNĐ, Phần trăm giảm giá: ${gDiscount.phanTramGiamGia}%
        `
    );
}
// hàm gọi api tạo đơn hàng mới
function callAPiCreateOrder() {
    $.ajax({
        url: gURL_ORDER,
        type: "POST",
        data: JSON.stringify(gSelectPerson),
        contentType: "application/json",
        success: function (res) {
            gNewOrder = res;
            // load dữ liệu vào modal
            loadOrderIdToModal()
        },
        error: function (res) {
            console.log(res.status);
        }
    })
}
// hàm gán order id vào form modal
function loadOrderIdToModal() {
    $("#inp-orderid").val(gNewOrder.orderId);
}
