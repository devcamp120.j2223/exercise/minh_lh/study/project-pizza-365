// khởi tạo bộ thư viện
const express = require('express');

//khởi tạo router
const router = express.Router();

router.get("/order", (request, response) => {
    response.json({
        message: "GET ALL Order"
    })
})

router.post("/order", (request, response) => {
    response.json({
        message: "POST new Order"
    })
})

router.get("/order/:orderId", (request, response) => {
    let orderId = request.params.orderId

    response.json({
        message: "GET order ID = " + orderId
    })
})

router.put("/order/:orderId", (request, response) => {
    let orderId = request.params.orderId

    response.json({
        message: "PUT order ID = " + orderId
    })
})

router.delete("/order/:orderId", (request, response) => {
    let orderId = request.params.orderId

    response.json({
        message: "DELETE order ID = " + orderId
    })
})

// export dữ liệu thành 1 module
module.exports = router;