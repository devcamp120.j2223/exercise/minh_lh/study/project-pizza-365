// khởi tạo bộ thư viện
const express = require('express');

const { createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById } = require('../controller/voucherController')

//khởi tạo router
const router = express.Router();

router.get("/voucher", getAllVoucher)

router.post("/voucher", createVoucher)

router.get("/voucher/:voucherId", getVoucherById)

router.put("/voucher/:voucherId", updateVoucherById)

router.delete("/voucher/:voucherId", deleteVoucherById)

// export dữ liệu thành 1 module
module.exports = router;