// khởi tạo bộ thư viện
const express = require('express');

//khởi tạo router
const router = express.Router();

router.get("/user", (request, response) => {
    response.json({
        message: "GET ALL User"
    })
})

router.post("/user", (request, response) => {
    response.json({
        message: "POST new User"
    })
})

router.get("/user/:userId", (request, response) => {
    let userId = request.params.userId

    response.json({
        message: "GET user ID = " + userId
    })
})

router.put("/user/:userId", (request, response) => {
    let userId = request.params.userId

    response.json({
        message: "PUT user ID = " + userId
    })
})

router.delete("/user/:userId", (request, response) => {
    let userId = request.params.userId

    response.json({
        message: "DELETE user ID = " + userId
    })
})

// export dữ liệu thành 1 module
module.exports = router;